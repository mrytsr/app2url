package com.example.shareapp.control;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.example.shareapp.data.AppInfoItem;
import com.example.shareapp.data.TriggerID;
import com.example.shareapp.data.TriggerInfo;
import com.example.shareapp.tools.AppTypeTool;
import com.example.shareapp.tools.Utilities;
import com.example.shareapp.ui.ExistsChecker;

public class AppDataHelper {
	public static AppDataHelper instance = null;
	private ArrayList<AppInfoItem> appInfoItemList = new ArrayList<AppInfoItem>();
	private AppDataHelper(){}
	public static AppDataHelper getInstance(){
		if(instance == null){
			instance = new AppDataHelper();
		}
		return instance;
	}
	public ArrayList<AppInfoItem> getInstalledPackageTitles(Context context){
		appInfoItemList.clear();
		final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);		
		final List<ResolveInfo> packages = context.getPackageManager().queryIntentActivities( mainIntent, 0);
		for(ResolveInfo packageInfo : packages){
			//filter system application 
			if((packageInfo.activityInfo.flags & ApplicationInfo.FLAG_SYSTEM)==0){		
				String pkgName =  packageInfo.activityInfo.packageName;
				if(AppTypeTool.checkAppType(pkgName, context) == AppTypeTool.SYSTEM_REF_APP){
					continue;
				}
				String appName = packageInfo.activityInfo.loadLabel(context.getPackageManager()).toString();
				Drawable d = packageInfo.activityInfo.loadIcon(context.getPackageManager());
				Bitmap bitmap = Utilities.createIconBitmap(d, context);
//				Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
				
				// tjx 支持添加自己
				AppInfoItem appitem = new AppInfoItem(appName, pkgName,bitmap);
				appInfoItemList.add(appitem);
				TriggerInfo triggerInfo = new TriggerInfo(TriggerID.MESSAGE_UPDATE_SCREEN);
				HandlerControl.getInstance().sendTrigger(triggerInfo);
			}
		}
		
		initCloudStatus();
		
		// tjx 添加代码支持排序
		Collections.sort(appInfoItemList, new Comparator<AppInfoItem>() {
			@Override
			public int compare(AppInfoItem o1, AppInfoItem o2) {
				return (o1.isExistOnCloud() ^ o2.isExistOnCloud()) ? (o1.isExistOnCloud() ? -1 : 1) : o1.getAppname().compareTo( o2.getAppname());
			}
		});
		// tjx end
		
		return appInfoItemList;
	}
	
	public void initCloudStatus(){
		Log.i("tag", "initCloudStatus");
		ArrayList<String> pnames = new ArrayList<String>();
		for(AppInfoItem appitem : appInfoItemList){
			pnames.add(appitem.getPkgname());
		}
		// 1. 查询是否存在
		ExistsChecker ec = new ExistsChecker(pnames);
		HashMap<String, Number> map = ec.check();
		if(map != null){
			ArrayList<AppInfoItem> appInfoItemList_temp = new ArrayList<AppInfoItem>();
			for(AppInfoItem appitem : appInfoItemList){
				map.get(appitem.getPkgname());
				if(map.get(appitem.getPkgname()).intValue() == 1){
					appitem.setExistOnCloud(true);
					appInfoItemList_temp.add(0, appitem);
				}else{
					appitem.setExistOnCloud(false);
					appInfoItemList_temp.add(0, appitem);
				}
			
			}
			appInfoItemList = appInfoItemList_temp;
			TriggerInfo triggerInfo = new TriggerInfo(TriggerID.MESSAGE_INIT_CLOUD_COMPLATED);
			HandlerControl.getInstance().sendTrigger(triggerInfo);
		}else{
			
		}
	
	}
	
	public AppInfoItem getAppInfoItem(String appname){
		for(AppInfoItem appitem : appInfoItemList){
			if(appname.equals(appitem.getAppname())){
				return appitem;
			}
		}
		return null;
	}
	public ArrayList<AppInfoItem> getAppInfoItemList(){
		return appInfoItemList;
	}
	
}
