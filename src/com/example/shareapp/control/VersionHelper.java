package com.example.shareapp.control;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.app2url.R;
import com.example.shareapp.ui.ShareActivity;

import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.StrictMode;
import android.util.Log;


public class VersionHelper {
	
	PackageManager manager = null;
	String pname = null;
	int currVcode = 0;
	int newVcode = 0;
	String currVname = "";
	String newVname = "";
	
	public void VersionHelper(){
		
	}
	
	public void setPackageManager(PackageManager manager){
		this.manager = manager;
	}
	
	public void setPname(String pname){
		this.pname = pname;
	}
	
	public int getCurrVcode(){
		PackageManager manager = this.manager;
		int vcode = 0;// = info.versionCode;
		String vname = "";
		try {
			PackageInfo info = manager.getPackageInfo(this.pname, 0);
			vname = info.versionName; // 版本名，versionCode同理
			vcode = info.versionCode;
		} catch (NameNotFoundException e) {
			Log.v("tjx", e.getMessage());
			e.printStackTrace();
		}
		this.currVcode = vcode;
		this.currVname = vname;
		return vcode;
//		Log.v("tjx", "vname = " + vname + "vcode " + vcode.toString());
	}
	
	@SuppressLint("NewApi")
	public int getNewVcode(){
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
		.detectDiskReads().detectDiskWrites().detectNetwork()
		.penaltyLog().build());
		int vcode = 0;
		String vname = "";
		try {
		        //得到DOM解析器的工厂实例  
		        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();  
		        //从DOM工厂中获得DOM解析器  
		        DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();  
		        //声明为File为了识别中文名  
		        Document doc = null;  
		        doc = dbBuilder.parse(ShareActivity.url_androidmainifest_online);  
		          
		        //得到文档名称为Student的元素的节点列表  
		        NodeList list = doc.getElementsByTagName("manifest");  
		        
		        //遍历该集合，显示结合中的元素及其子元素的名字  
		        for(int i = 0; i< list.getLength() ; i ++){  
		            Element element = (Element)list.item(i);  
		            String str = element.getAttribute("android:versionCode");  
		            vname = element.getAttribute("android:versionName");  
		            vcode = Integer.parseInt(str);
			        Log.v("tjx", "vcode:" + str);
		        }   
		} catch (ParserConfigurationException e) {
		        Log.v("tjx", e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
		        Log.v("tjx", e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
		        Log.v("tjx", e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.newVcode = vcode;
		this.newVname = vname;
		return vcode;
	}
	
	public String getVersionAlert(){
		Log.v("tjx", "currVcode: " + String.valueOf(this.getCurrVcode()));
		Log.v("tjx", "newVcode: " + String.valueOf(this.getNewVcode()));
		if(this.currVcode >= this.newVcode){
			return "";
		}else{
			return "Current verion is " + this.currVname + ", Newest version is " + this.newVname;
		}
	}
	
	public String getDownloadUrl(){
		return ShareActivity.url_new_version_apk; 
	}
	
}
