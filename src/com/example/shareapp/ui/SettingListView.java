package com.example.shareapp.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.app2url.R;

public class SettingListView extends ListView{
	private String[] items = new String[]{"关于"};
	private MyAdpter myAdpter;
	@SuppressLint("ResourceAsColor")
	public SettingListView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		setBackgroundColor(R.color.window_background);
		myAdpter = new MyAdpter();
		this.setAdapter(myAdpter);
	}
	public void notifychanged(){
		myAdpter.notifyDataSetChanged();
	}
	
	private class MyAdpter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return items.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView != null){
				((SettringItemView)convertView).setTitle(items[position]);
			}else{
				convertView = new SettringItemView(getContext(), items[position]);
			}
			return convertView;
		}
		
	}

}
