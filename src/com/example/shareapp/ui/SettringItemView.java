package com.example.shareapp.ui;


import com.app2url.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettringItemView extends RelativeLayout{
	private TextView tv = null;
	public SettringItemView(Context context,String title) {
		super(context);
		// TODO Auto-generated constructor stub
		LayoutInflater inflater = (LayoutInflater)context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		View view = inflater.inflate( R.layout.setting_list_item , this );
		tv = (TextView)view.findViewById(R.id.title);
		tv.setText(title);
	}
	 
	public void setTitle(String title){
		tv.setText(title);
	}
}
