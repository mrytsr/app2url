package com.example.shareapp.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app2url.R;

public class AppViewExternal extends RelativeLayout{
	private ImageView image = null;
	private TextView appName = null;
	private ImageView cloudIcon = null;
	public AppViewExternal(Context context,Bitmap appImage,String name,boolean isexistCloud) {
		super(context);
		// TODO Auto-generated constructor stub
		LayoutInflater inflater = (LayoutInflater)context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		View view = inflater.inflate( R.layout.grid_item_external , this );
		image = (ImageView) view.findViewById(R.id.appicon);
		appName = (TextView) view.findViewById(R.id.appname);
		cloudIcon = (ImageView) view.findViewById(R.id.cloud_icon);
		image.setImageBitmap(appImage);
		appName.setText(name);
		setIsAppExist(isexistCloud);
	
	}
	public void setIsAppExist(boolean isexist){
		if(isexist){
			cloudIcon.setVisibility(View.VISIBLE);
		}else{
			cloudIcon.setVisibility(View.INVISIBLE);
		}
	}
	public void setAppName(String appname){
		appName.setText(appname);
	}
	public void setAppImage(Bitmap bitmap){
		image.setImageBitmap(bitmap);
	}
}
