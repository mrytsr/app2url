package com.example.shareapp.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import com.app2url.R;
import com.example.shareapp.control.AppDataHelper;
import com.example.shareapp.control.HandlerControl;
import com.example.shareapp.control.VersionHelper;
import com.example.shareapp.tools.NetWorkTool;


public class ShareActivity extends Activity{
	private VersionHelper vh;
	public static String url_check_exists;
	public static String url_share_prefix;
	public static String url_new_version_apk;
	public static String url_androidmainifest_online;
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		// tjx 在次载入配置变量
		ShareActivity.url_check_exists = getResources().getString(R.string.url_check_exists);
		ShareActivity.url_share_prefix = getResources().getString(R.string.url_share_prefix);
		ShareActivity.url_new_version_apk = getResources().getString(R.string.url_new_version_apk);
		ShareActivity.url_androidmainifest_online = getResources().getString(R.string.url_androidmainifest_online);
		
		super.onCreate(savedInstanceState);
		if(!NetWorkTool.isNetworkAvailable(this)){
			Toast.makeText(this, R.string.strat_app_check_net_tips, 3000).show();
			finish();
		}
		HandlerControl.getInstance();
		new Thread(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				super.run();
				AppDataHelper.getInstance().getInstalledPackageTitles(ShareActivity.this);
			}
			
		}.start();
		setContentView(R.layout.activity_main);
		
		// tjx VersionHelper
		vh = new VersionHelper();
		vh.setPackageManager(this.getPackageManager());
		vh.setPname(this.getPackageName());
		String vAlert = vh.getVersionAlert();
		if(vAlert == ""){
			Log.v("tjx", "VersionAlert: " + "You have the newest version");
//			popUpdateWindow();
		}else{
			popUpdateWindow();
			Log.v("tjx", "VersionAlert: " + vAlert);
			Log.v("tjx", "DownloadUrl: " + vh.getDownloadUrl());
		}
		
//		// tjx ExistsCheck
//		ArrayList<String> pnames = new ArrayList<String>();
//		pnames.add("org.wikipedia");
//		pnames.add("xxx.wikipedia");
//		
//		// 1. 查询是否存在
//		ExistsChecker ec = new ExistsChecker(pnames);
//		HashMap<String, Number> map = ec.check();
//		Log.v("tjx", map.toString());
//		
//		// 2. 获取短分享链接
//		String pname = "org.wikipedia";
//		Log.v("tjx", pname + ": " + "http://sapk.tjx.be/s.php?m=" + ExistsChecker.getMda(pname));
	}

	
	private void popUpdateWindow() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(ShareActivity.this);
		builder.setMessage(R.string.dialog_new_version_content)
		       .setTitle(R.string.dialog_new_version_tips);
		builder.setPositiveButton(R.string.dialog_new_version_yes, new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		               // User clicked OK button
		        	   Intent i = new Intent(Intent.ACTION_VIEW , Uri.parse(vh.getDownloadUrl()));
		        	   startActivity(i);

		           }
		       });
		builder.setNegativeButton(R.string.dialog_new_version_no, new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		               // User cancelled the dialog
		       }
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		HandlerControl.getHandlerControl().arrayList.clear();
		super.onDestroy();
	}
	
	
}
